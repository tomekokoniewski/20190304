package pl.com.tok.mavenproject1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tomek
 */
public class ParkMaszynowy {

    public static void main(String[] args) {

        ArrayList<Maszyna> maszyny = new ArrayList<>();

        maszyny.add(new Kosiarka("ST2", "Bosh", 20, RodzajSilnika.BENZYNA, false, true, 2));
        maszyny.add(new Kosiarka("ST3", "Bosh", 25, RodzajSilnika.BENZYNA, false, true, 4));
        maszyny.add(new Lokomotywa("MC2", "Pendolino", 7200, RodzajSilnika.DIESEL, false, 20));
        maszyny.add(new Lokomotywa("MC3", "Pendolino", 7500, RodzajSilnika.DIESEL, false, 25));
        maszyny.add(new Samochod("VW", "Samochód osobowy", 1000, RodzajSilnika.BENZYNA, 90.1, 200.0, "Polo", "VC122123ED123"));
        maszyny.add(new Samochod("VW", "Samochód osobowy", 1800, RodzajSilnika.BENZYNA, 190.2, 200.0, "Passat", "VC22222123ED123"));
        maszyny.add(new Jednoslad("MT1", "Motorower", 50, RodzajSilnika.BENZYNA, 12.3, 30.1, "Komar"));
        maszyny.add(new Jednoslad("MT2", "Motorower", 50, RodzajSilnika.BENZYNA, 12.0, 30.2, "Romet"));

        for (Maszyna maszyna : maszyny) {
            maszyna.wyswietl();
        }
        System.out.println("----------------");

        for (Maszyna maszyna : maszyny) {
            if (maszyna instanceof Kosiarka) {
                ((Kosiarka) maszyna).setLiczbaOstrzy(3);
            }
        }
        for (Maszyna maszyna : maszyny) {
            maszyna.wyswietl();
        }

      Statistics.calculate(maszyny);
    }
}
