package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public abstract class Maszyna {
    
    protected String marka;
    protected String nazwa;
    protected int pojemnoscSilnika;
    protected RodzajSilnika rodzajSilnika;
     
    abstract void wyswietl();
}
