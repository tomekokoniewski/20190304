package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public class Lokomotywa extends Maszyna {
    
    boolean czyRadziecka;
    int maxLiczbaWagonow;

    public Lokomotywa(String nazwa, String marka, int pojemnoscSilnika, RodzajSilnika rodzajSilnika, boolean czyRadziecka, int maxLiczbaWagonow) {
        
        this.marka = marka;
        this.nazwa = nazwa;
        this.pojemnoscSilnika = pojemnoscSilnika;
        this.rodzajSilnika = rodzajSilnika;
        this.czyRadziecka = czyRadziecka;
        this.maxLiczbaWagonow = maxLiczbaWagonow;
    }
 
    @Override
    public void wyswietl() {
        System.out.println("Pojazd: \n"+"Marka "+marka+"\n Nazwa "+nazwa
                +"\n Poj. silnika "+Integer.toString(pojemnoscSilnika)+"/n Rodz. silnika "+rodzajSilnika.toString()
                +"\n Radziecka "+czyRadziecka+"\n Max liczba wagonów "+Integer.toString(maxLiczbaWagonow));
    } 
}
