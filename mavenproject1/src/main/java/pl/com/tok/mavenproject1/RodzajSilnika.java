
package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public enum RodzajSilnika {
   DIESEL,
   BENZYNA,
   HYBRYDA,
   ELEKTRYCZNY;
   
}
