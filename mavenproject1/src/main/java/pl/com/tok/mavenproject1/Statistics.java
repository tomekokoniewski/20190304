package pl.com.tok.mavenproject1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author tomek
 */
public class Statistics {

    public static void calculate(ArrayList<Maszyna> maszyny) {

        HashMap<String, ArrayList<Double>> stat = new HashMap<>();
        HashMap<String, Integer> occur = new HashMap<>();

        for (Maszyna maszyna : maszyny) {
            ArrayList<Double> statystyka = new ArrayList<>();

            if (maszyna instanceof Pojazd) {
                if (!stat.keySet().contains(maszyna.getClass().getName())) {
                    statystyka.add(((Pojazd) maszyna).moc);
                    statystyka.add(((Pojazd) maszyna).momentObrotowy);
                    stat.put(maszyna.getClass().getName(), statystyka);
                    occur.put(maszyna.getClass().getName(), 1);
                } else {
                    statystyka.add(((stat.get(maszyna.getClass().getName()).get(0)) + ((Pojazd) maszyna).moc)
                            / (occur.get(maszyna.getClass().getName()) + 1)); //wyliczenie średniej mocy
                    statystyka.add(((stat.get(maszyna.getClass().getName()).get(1)) + ((Pojazd) maszyna).momentObrotowy)
                            / (occur.get(maszyna.getClass().getName()) + 1)); //wyliczenie średniego momentu
                    stat.put(maszyna.getClass().getName(), statystyka); // dodanie statystyk
                    occur.put(maszyna.getClass().getName(), occur.get(maszyna.getClass().getName()) + 1); //zaktualizowanie wystąpień
                }
            }
        }

        for (Map.Entry<String, ArrayList<Double>> entry : stat.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
