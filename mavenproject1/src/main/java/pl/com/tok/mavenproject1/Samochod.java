package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public class Samochod extends Pojazd{
    
    String model;
    String vin;
    
    Samochod(String marka, String nazwa, int pojemnoscSilnika, RodzajSilnika rodzajSilnika, Double moc, Double momentObrotowy, String model, String vin){
        super(marka,nazwa,pojemnoscSilnika,rodzajSilnika,moc,momentObrotowy);
        this.model = model;
        this.vin = vin;
    }
    
    @Override
    public void wyswietl() {
        System.out.println("Pojazd: \n"+"Marka "+marka+"\n Nazwa "+nazwa
                +"\n Poj. silnika "+Integer.toString(pojemnoscSilnika)+"/n Rodz. silnika "+rodzajSilnika.toString()
                +"\n Moc "+moc+"\n Moment obr. "+momentObrotowy
                +"\n Model "+model+" \n VIN "+vin+"\n Nr. pojazdu "+nrPojazdu+"/"+Pojazd.getMaxLiczbaPojazdow());
    }
}
