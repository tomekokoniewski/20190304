package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public class Kosiarka extends Maszyna {
    
    boolean czyMelekser;
    boolean czyNaped;
    int liczbaOstrzy;

    public Kosiarka(String nazwa, String marka, int pojemnoscSilnika, RodzajSilnika rodzajSilnika, boolean czyMelekser, boolean czyNaped, int liczbaOstrzy) {
        
        this.marka = marka;
        this.nazwa = nazwa;
        this.pojemnoscSilnika = pojemnoscSilnika;
        this.rodzajSilnika = rodzajSilnika;
        this.czyMelekser = czyMelekser;
        this.czyNaped = czyNaped;
        this.liczbaOstrzy = liczbaOstrzy;
    }
    
    public void setCzyMelekser(boolean czyMelekser) {
        this.czyMelekser = czyMelekser;
    }

    public void setCzyNaped(boolean czyNaped) {
        this.czyNaped = czyNaped;
    }

    public void setLiczbaOstrzy(int liczbaOstrzy) {
        this.liczbaOstrzy = liczbaOstrzy;
    }

    @Override
    public void wyswietl() {
        System.out.println("Pojazd: \n"+"Marka "+marka+"\n Nazwa "+nazwa
                +"\n Poj. silnika "+Integer.toString(pojemnoscSilnika)+"/n Rodz. silnika "+rodzajSilnika.toString()
                +"\n Melekser "+czyMelekser+"\n Napęd "+czyNaped+"\n Liczba ostrzy "+liczbaOstrzy);
    } 
}
