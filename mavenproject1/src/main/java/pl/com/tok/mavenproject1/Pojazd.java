package pl.com.tok.mavenproject1;
/**
 * @author tomek
 */
public class Pojazd extends Maszyna {

    protected static int maxLiczbaPojazdow;
    protected Double moc;
    protected Double momentObrotowy;
    protected int nrPojazdu;

    public Pojazd(String marka, String nazwa, int pojemnoscSilnika, RodzajSilnika rodzajSilnika, Double moc, Double momentObrotowy) {

        this.marka = marka;
        this.nazwa = nazwa;
        this.pojemnoscSilnika = pojemnoscSilnika;
        this.rodzajSilnika = rodzajSilnika;
        this.moc = moc;
        this.momentObrotowy = momentObrotowy;
 
        this.nrPojazdu = ++maxLiczbaPojazdow;
    }

    public void wyswietl() {
        System.out.println("Pojazd: \n"+"Marka "+marka+"\n Nazwa "+nazwa
                +"\n Poj. silnika "+Integer.toString(pojemnoscSilnika)+"/n Rodz. silnika "+rodzajSilnika.toString()
                +"\n Moc "+moc+"\n Moment obr. "+momentObrotowy);
    }

    public static int getMaxLiczbaPojazdow() {
        return maxLiczbaPojazdow;
    }

    public int getNrPojazdu() {
        return nrPojazdu;
    }

    
    
    
}

