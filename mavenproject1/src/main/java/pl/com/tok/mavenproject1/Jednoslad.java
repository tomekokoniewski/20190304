package pl.com.tok.mavenproject1;

/**
 * @author tomek
 */
public class Jednoslad extends Pojazd{
    
    String typ;
    
    Jednoslad(String marka, String nazwa, int pojemnoscSilnika, RodzajSilnika rodzajSilnika, Double moc, Double momentObrotowy, String typ){
        super(marka,nazwa,pojemnoscSilnika,rodzajSilnika,moc,momentObrotowy);
        this.typ = typ;
    }
    
    @Override
    public void wyswietl() {
        System.out.println("Pojazd: \n"+"Marka "+marka+"\n Nazwa "+nazwa
                +"\n Poj. silnika "+Integer.toString(pojemnoscSilnika)+"/n Rodz. silnika "+rodzajSilnika.toString()
                +"\n Moc "+moc+"\n Moment obr. "+momentObrotowy
                +"\n Typ "+typ+"\n Nr. pojazdu "+nrPojazdu+"/"+Pojazd.getMaxLiczbaPojazdow());
    }
}
